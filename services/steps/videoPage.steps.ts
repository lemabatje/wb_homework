import { Page } from 'playwright'
import VideoPage from '../pages/videoPage'


export default class VideoPageSteps {

  private page: Page
  private videoPage: VideoPage

  constructor(page: Page, videoPage: VideoPage) {
    this.page = page
    this.videoPage = videoPage
  }

  async insertQueryText(queryText: string) {
    await this.videoPage.getInput().isVisible()
    await this.videoPage.getInput().fill(queryText)
  }

  async clickSearchButton() {
    await this.videoPage.getSearchButton().isVisible()
    await this.videoPage.getSearchButton().click()
  }

  async waitForResults() {
    await this.page.waitForLoadState('load')
    await this.videoPage.getSearchedResults().isVisible()
  }

  async getFirstVideoBlock() {
    await this.videoPage.firstItem('inactive').isVisible()
  }

  async changePreviewMode() {
    await this.videoPage.firstItem('inactive').hover( {force: true} )
    await this.videoPage.firstItem('active').isVisible()
  }
}
