import { Locator, Page } from '@playwright/test'
import * as selectors from '../../selectors.json'


export default class VideoPage {

  private page: Page

  constructor(page: Page) {
    this.page = page;
  }

  public getInput(): Locator {
    return this.page.locator(selectors.videoPage.input)
  }

  public getSearchButton(): Locator {
    return this.page.locator(selectors.videoPage.searchButton)
  }

  public getSearchedResults(): Locator {
    return this.page.locator(selectors.videoPage.searchedResultsContainer)
  }

  public firstItem(previewMode: 'inactive' | 'active'): Locator {
    let itemBlock: string
    switch (previewMode) {
    case 'inactive':
      itemBlock = selectors.videoPage.item.inactive
      break
    case 'active':
      itemBlock = selectors.videoPage.item.active
      break
    }
    return this.page.locator(itemBlock).first()
  }

}
