import { test, firefox } from '@playwright/test'
import * as videoPageData from '../services/data/videoPage.data.json'
import VideoPageSteps from '../services/steps/videoPage.steps'
import VideoPage from '../services/pages/videoPage'

test.describe('Test suite', () => {
    
    let videoPage: VideoPage
    let videoPageSteps: VideoPageSteps

    test.beforeAll(async () => {
        const browser = await firefox.launch()
        const page = await browser.newPage()
        videoPage = new VideoPage(page)
        videoPageSteps = new VideoPageSteps(page, videoPage)

        await page.goto(videoPageData.data.baseURL, {waitUntil: 'load'})
    })

    test('Entering a search query', async () => {
        await videoPageSteps.insertQueryText(videoPageData.data.queryText)
    })

    test('Clicking the search button', async () => {
        await videoPageSteps.clickSearchButton()
    })

    test('Waiting for search results', async () => {
        await videoPageSteps.waitForResults()
    })

    test('Checking first output item', async () => {
        await videoPageSteps.getFirstVideoBlock()
    })

    test('Preview activation', async () => {
        await videoPageSteps.changePreviewMode()
    })
})
