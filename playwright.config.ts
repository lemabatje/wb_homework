import { PlaywrightTestConfig } from "@playwright/test"

export const config: PlaywrightTestConfig = {

    expect: {
        timeout: 5000
    },

    use: {
        baseURL: 'URL',
        headless: true,
        javaScriptEnabled: true
    },

    retries: 0
}

export default config
